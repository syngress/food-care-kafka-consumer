package avro.foodcare.consumer.download;

import com.mongodb.*;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import lombok.extern.apachecommons.CommonsLog;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.text.SimpleDateFormat;
import java.util.Date;

import avro.foodcare.consumer.GetEmailMessage;

@Service
@CommonsLog(topic = "Consumer Log")
public class EmailMessageConsumer {

    @Value("${topic.email-messages}")
    private String topicEmailMessages;

    @Value("${spring.data.mongodb.database}")
    private String databaseName;

    private static final String TOPIC_NAME = "${topic.email-messages}";
    private static final String GROUP_ID = "${spring.kafka.consumer.group-id}";

    @KafkaListener(topics = TOPIC_NAME, groupId = GROUP_ID)
    public void consume(ConsumerRecord<String, GetEmailMessage> record) {
        log.info(String.format("Consumed message -> %s", record.value()));
        processMessage(record);
    }

    private void processMessage(final ConsumerRecord<String, GetEmailMessage> record) {
        var mongoClient = MongoClients.create();
        var database = mongoClient.getDatabase(databaseName);
        MongoCollection<Document> collection = database.getCollection("emailMessage");

        try {
            String data = String.format("%s", record.value());
            JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
            JsonObject payload = jsonObject.getAsJsonObject("payload");

            int messageDate = payload.get("dispatch_time").getAsInt();
            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .format(new Date(messageDate * 1000L));

            Document message = new Document()
                    .append("_id", ObjectId.get())
                    .append("topic", record.topic())
                    .append("email", payload.get("email").getAsString())
                    .append("subject", payload.get("subject").getAsString())
                    .append("restaurant_name", payload.get("restaurant_name").getAsString())
                    .append("message", payload.get("message").getAsString())
                    .append("date", date);
            collection.insertOne(message);
        } catch (MongoException e) {
            e.printStackTrace();
        }
    }
}
