package avro.foodcare.consumer.validator;

import static avro.foodcare.consumer.exception.ExceptionKey.notFound;
import static avro.foodcare.consumer.validator.Validate.isTrue;

public class EmailMessageValidator {
    public void emailMessageExist(Boolean emailMessageObject) {
        isTrue(emailMessageObject, notFound, "Email message not found");
    }
}
