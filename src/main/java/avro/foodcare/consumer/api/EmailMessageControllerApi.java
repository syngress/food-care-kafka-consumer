package avro.foodcare.consumer.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import avro.foodcare.consumer.model.vo.EmailMessageVO;
import avro.foodcare.consumer.model.ErrorResponse;

@Api(value = "v1")
public interface EmailMessageControllerApi {
    // Get Email Message Object
    @ApiOperation(value = "", nickname = "getEmailMessage", notes = "Get Email Message",
            response = EmailMessageVO.class, tags = {"get"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = EmailMessageVO.class),
            @ApiResponse(code = 400, message = "BadRequest", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "NotFound", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)})
    @GetMapping("/v1/email_message/{id}")
    EmailMessageVO getEmailMessageById(ObjectId id);
}
