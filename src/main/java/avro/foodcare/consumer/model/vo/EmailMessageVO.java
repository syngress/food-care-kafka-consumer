package avro.foodcare.consumer.model.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.mongodb.core.index.Indexed;

public class EmailMessageVO {
    @JsonProperty("id")
    @ApiModelProperty(required = true, value = "Object ID")
    public String id;

    @JsonProperty("topic")
    @ApiModelProperty(required = true, value = "Topic")
    public String topic;

    @JsonProperty("email")
    @Indexed(unique = true)
    @ApiModelProperty(required = true, value = "Email Address")
    public String email;

    @JsonProperty("subject")
    @ApiModelProperty(required = true, value = "Subject")
    public String subject;

    @JsonProperty("restaurant_name")
    @ApiModelProperty(required = true, value = "Restaurant Name")
    public String restaurant_name;

    @JsonProperty("message")
    @ApiModelProperty(required = true, value = "Message")
    public String message;

    @JsonProperty("date")
    @ApiModelProperty(required = true, value = "Date")
    public String date;

}
