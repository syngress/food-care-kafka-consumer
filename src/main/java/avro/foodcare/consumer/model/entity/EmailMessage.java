package avro.foodcare.consumer.model.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class EmailMessage {
    @Id
    private ObjectId id;
    private String topic;
    private String email;
    private String subject;
    private String restaurant_name;
    private String message;
    private String date;

    public EmailMessage() {}

    public EmailMessage(ObjectId id, String topic, String email, String subject, String restaurant_name, String message, String date) {
        this.id = id;
        this.topic = topic;
        this.email = email;
        this.subject = subject;
        this.restaurant_name = restaurant_name;
        this.message = message;
        this.date = date;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
