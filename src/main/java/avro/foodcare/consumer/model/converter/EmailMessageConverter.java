package avro.foodcare.consumer.model.converter;

import avro.foodcare.consumer.model.entity.EmailMessage;
import avro.foodcare.consumer.model.vo.EmailMessageVO;
import org.bson.types.ObjectId;

public class EmailMessageConverter {

    private EmailMessageConverter() {
    }

    public static EmailMessage toEmailMessage(EmailMessageVO vo) {
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setId(new ObjectId());
        emailMessage.setTopic(vo.topic);
        emailMessage.setEmail(vo.email);
        emailMessage.setSubject(vo.subject);
        emailMessage.setRestaurant_name(vo.restaurant_name);
        emailMessage.setMessage(vo.message);
        emailMessage.setDate(vo.date);

        return emailMessage;
    }

    public static EmailMessageVO toValueObject(EmailMessage emailMessage) {
        EmailMessageVO vo = new EmailMessageVO();
        vo.id = emailMessage.getId();
        vo.topic = emailMessage.getTopic();
        vo.email = emailMessage.getEmail();
        vo.subject = emailMessage.getSubject();
        vo.restaurant_name = emailMessage.getRestaurant_name();
        vo.message = emailMessage.getMessage();
        vo.date = emailMessage.getDate();

        return vo;
    }
}
