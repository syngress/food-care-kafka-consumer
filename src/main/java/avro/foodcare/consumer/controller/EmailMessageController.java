package avro.foodcare.consumer.controller;

import avro.foodcare.consumer.service.EmailMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import avro.foodcare.consumer.api.EmailMessageControllerApi;
import avro.foodcare.consumer.model.converter.EmailMessageConverter;
import avro.foodcare.consumer.model.entity.EmailMessage;
import avro.foodcare.consumer.model.vo.EmailMessageVO;

@RestController
public class EmailMessageController implements EmailMessageControllerApi {

    @Autowired
    private EmailMessageService service;

    public EmailMessageVO getEmailMessageById(@PathVariable("id") ObjectId id) {
        EmailMessage emailMessage = service.getEmailMessageById(id);
        return EmailMessageConverter.toValueObject(emailMessage);
    }
}
