package avro.foodcare.consumer.repository;

import avro.foodcare.consumer.model.entity.EmailMessage;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmailMessageRepository extends MongoRepository<EmailMessage, String> {
    EmailMessage findById(ObjectId id);
}
