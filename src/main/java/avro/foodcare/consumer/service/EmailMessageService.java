package avro.foodcare.consumer.service;

import org.bson.types.ObjectId;
import avro.foodcare.consumer.model.entity.EmailMessage;

public interface EmailMessageService {
    EmailMessage getEmailMessageById(ObjectId id);
}
