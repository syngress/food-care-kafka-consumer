package avro.foodcare.consumer.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.bson.types.ObjectId;

import avro.foodcare.consumer.model.entity.EmailMessage;
import avro.foodcare.consumer.repository.EmailMessageRepository;
import avro.foodcare.consumer.validator.Validate;

@Service
public class EmailMessageServiceImpl implements EmailMessageService {

    @Autowired
    private EmailMessageRepository emailMessageRepository;

    @Override
    @Transactional
    public EmailMessage getEmailMessageById(ObjectId id) {
        EmailMessage emailMessageObject = emailMessageRepository.findById(id);
        validateEmailMessageObject(emailMessageObject);
        return emailMessageObject;
    }

    private void validateEmailMessageObject(final EmailMessage emailMessageObject) {
        Validate.emailMessageValidator.emailMessageExist(emailMessageObject != null);
    }
}
