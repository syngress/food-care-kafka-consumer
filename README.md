# Foodcare Kafka Consumer

![FCAjava](https://syngress.pl/images/foodcare_kafka_consumer/java.png)![GoRestApiKafka](https://syngress.pl/images/foodcare_kafka_consumer/apache-kafka.png)


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Foodcare Kafka Consumer allows you to download data directly from the selected Kafka Topic, and send them to Mongo Database.  
You can run Mongo Database directly from Docker using included docker-compose.  
This is consumer application for *Foodcare Api* project: https://bitbucket.org/syngress/food-care-api/src/master/  

# Things you may want to cover for local development:

  - Java from `13.0.1` to `15.0.3`
  - SpringBoot version `2.2.2`
  - Apache Maven `3.6.3`
  - SpringKafka `2.4.0`
  - Avro `1.9.1`
  - KafkaAvroSerializer `5.3.0`
  - MonitoringInterceptors `5.3.0`
  - Docker `20.10.8`
  - docker-compose `1.29.2`

# Preparation

- Setup your application configuration file `src/main/resources/application.yaml`
- We have created appropriate Avro schema for FCA backend application. If you need your schema, place it in the `src/main/avro` directory.  
- Generate sources `mvn generate-sources`
- Build project `mvn clean package`
- You can test-run applications directly from the cmd: `java -jar target/your-project.jar`

# Description of functionalities

[Topic] is a category/feed name to which messages are stored and published.  
Messages are byte arrays that can store any object in any format.  
[Consumer] is used for optimal consumption of Kafka data. The primary role of a Kafka consumer is to take Kafka connection and consumer properties to read records from the appropriate Kafka broker.  
As soon as the application starts, Kafka receives information that a new consumer has appeared and start rebalance consumer group:  

- Kafka log:  

> ``` Preparing to rebalance group foodcare_avro_consumer in state PreparingRebalance with old generation 16 (__consumer_offsets-29) ```  
> ``` (reason: Adding new member consumer-1-0f5aa497-0785-4aed-af1d-db7fdb7b5ea9 with group instanceid None) (kafka.coordinator.group.GroupCoordinator)  ```

A consumer group is a group of related consumers that perform a task, like putting data into Hadoop or sending messages to a service  
Consumer groups each have unique offsets per partition. Different consumer groups can read from different locations in a partition.  
Correct connection will be indicated by the following message:  

- Consumer log:  

> ```[Consumer clientId=consumer-1, groupId=foodcare_avro_consumer] (Re-)joining group```  
> ```[Consumer clientId=consumer-1, groupId=foodcare_avro_consumer] Successfully joined group with generation 17```  
> ```[Consumer clientId=consumer-1, groupId=foodcare_avro_consumer] Setting newly assigned partitions: email-0```  
> ```[Consumer clientId=consumer-1, groupId=foodcare_avro_consumer] Setting offset for partition email-0 to the committed offset FetchPosition{offset=13```  
> ```offsetEpoch=Optional.emptycurrentLeader=LeaderAndEpoch{leader=kafka:9092 (id: 1 rack: null), epoch=0}}```

From now on, all serialized messages will be immediately downloaded and deserialized by consumer.  

# Data storage

Received data should be stored in Mongo Databse.  
Mongo document should be saved in presented structure  

```shell script
{
    "_id" : ObjectId("5e12745a98e07b2c35fca860"),
    "topic" : "email",
    "email" : "test@testowy.pl",
    "subject" : "See You Next Time",
    "restaurant_name" : "MyRestaurant",
    "message" : "See You Next Time In MyResturant",
    "date" : "2020-01-06 00:42:18"
}
```
### Table of endpoint

| Method | Endpoint                                   |Note                      |
| ------ | ------------------------------------------ |------------------------- |
| GET    | /v1/email_message/5e135d60152c5007f15e2a4f | Get email message object |

Backend: https://bitbucket.org/syngress/food-care-api/src/master/  
Forntend: https://bitbucket.org/syngress/food-care-client/src/master/

Enjoy ! :)
